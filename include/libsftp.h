#ifndef _LIBSFTP_H_
#define _LIBSFTP_H_

typedef struct libsftp_ses
{
    void *libssh2_session;
    void *libssh2_sftp_session;
    int socket;
} libsftp_session;


typedef struct libsftp_flst
{
    char *filename;
    unsigned long flags;
    unsigned long long filesize;
    unsigned long uid, gid;
    unsigned long permissions;
    unsigned long atime, mtime;
    unsigned char isdir;
    struct libsftp_flst *next;

} libsftp_filelist;

extern libsftp_session *libsftp_connect_pubkey(unsigned long hostaddr, const char *username, const char *publickey, const char *privatekey, const char *passphrase);
extern libsftp_session *libsftp_connect_password(unsigned long hostaddr, const char *username, const char *password);
extern int libsftp_get (libsftp_session *, const char *src_path, const char *dst_path);
extern int libsftp_put (libsftp_session *, const char *src_path, const char *dst_path);
extern libsftp_filelist* libsftp_opendir(libsftp_session *, const char *path);
extern void libsftp_disconnect(libsftp_session *sftp);
#endif
