#include <libsftp.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    libsftp_session *session;
    long hostaddr;
    char* dst;
    int rc;
    if (argc < 6)
    {
        fprintf (stderr,"Usage: sftptest ipaddr user_name public_key private_key src_file\n");
        return(-1);
    }
    hostaddr = inet_addr(argv[1]);
    session = libsftp_connect_pubkey(hostaddr, argv[2], argv[3], argv[4], argv[5]);
    if (session != NULL)
    {
        rc = libsftp_get(session, argv[6], "tmp.txt");
        if (rc >= 0)
        {
            dst = (char *) malloc (strlen(argv[6]) + 1);
            strcpy(dst, argv[6]);
            strcat(dst, "1");
            libsftp_put(session, "tmp.txt", dst);
            free(dst);
        }
        libsftp_disconnect(session);
    }
}
