#include <libsftp.h>
#include <libssh2.h>
#include <libssh2_sftp.h>
 
#ifdef WIN32
#include <winsock2.h>
#else
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>
#endif
 
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

#ifndef __PRI64_PREFIX
#ifdef WIN32
#define __PRI64_PREFIX "I64"
#else
#if __WORDSIZE == 64
#define __PRI64_PREFIX "l"
#else
#define __PRI64_PREFIX "ll"
#endif  
#endif  
#endif  
#ifndef PRIu64
#define PRIu64 __PRI64_PREFIX "u"
#endif  

static int libsftp_shutdown(libsftp_session *sftp)
{
    LIBSSH2_SESSION *session = (LIBSSH2_SESSION *)sftp->libssh2_session;
    libssh2_session_disconnect(session, "");
    libssh2_session_free(session);
 
#ifdef WIN32
    closesocket(sftp->socket);
#else
    close(sftp->socket);
#endif
#ifndef NDEBUG
    fprintf(stderr, "all done\n");
#endif
    libssh2_exit();
    return 0;    
}

static libsftp_session *libsftp_init_session(unsigned long hostaddr)
{
    LIBSSH2_SESSION *session;
    int rc;
    struct sockaddr_in sin;
    libsftp_session *sftp = (libsftp_session*)malloc(sizeof(libsftp_session));
#ifdef WIN32
    WSADATA wsadata;
    int err;
 
    err = WSAStartup(MAKEWORD(2,0), &wsadata);
    if (err != 0) {
#ifndef NDEBUG
        fprintf(stderr, "WSAStartup failed with error: %d\n", err);
#endif
        return NULL;
    }
#endif

    rc = libssh2_init (0);

    if (rc != 0) {
#ifndef NDEBUG
        fprintf(stderr, "libssh2 initialization failed (%d)\n", rc);
#endif
        return NULL;
    }

    sftp->socket = socket(AF_INET, SOCK_STREAM, 0);
 
    sin.sin_family = AF_INET;
    sin.sin_port = htons(22);
    sin.sin_addr.s_addr = hostaddr;
    if (connect(sftp->socket, (struct sockaddr*)(&sin),
                sizeof(struct sockaddr_in)) != 0) {
#ifndef NDEBUG
        fprintf(stderr, "failed to connect!\n");
#endif
        return NULL;
    }
    session = libssh2_session_init();

    if(!session)
        return NULL;
 
    /* Since we have set non-blocking, tell libssh2 we are blocking */ 
    libssh2_session_set_blocking(session, 1);

 
    /* ... start it up. This will trade welcome banners, exchange keys,
     * and setup crypto, compression, and MAC layers
     */ 
    rc = libssh2_session_handshake(session, sftp->socket);

    if(rc) {
#ifndef NDEBUG
        fprintf(stderr, "Failure establishing SSH session: %d\n", rc);
#endif
        return NULL;
    }
    sftp->libssh2_session = session;
    return (sftp);
}

    
int libsftp_put (libsftp_session *sftp, const char *src_path, const char *dst_path)
{
#ifndef NDEBUG
    fprintf(stderr, "PUT: %s -> %s\n", src_path, dst_path);
#endif
    LIBSSH2_SFTP *sftp_session = (LIBSSH2_SFTP  *)sftp->libssh2_sftp_session;
    LIBSSH2_SFTP_HANDLE *sftp_handle;
    int rc;
    
    //sftp_handle = libssh2_sftp_open(sftp_session, dst_path, LIBSSH2_FXF_WRITE, 0);
    sftp_handle = libssh2_sftp_open(sftp_session, dst_path,

                      LIBSSH2_FXF_WRITE|LIBSSH2_FXF_CREAT|LIBSSH2_FXF_TRUNC,
                      LIBSSH2_SFTP_S_IRUSR|LIBSSH2_SFTP_S_IWUSR|
                      LIBSSH2_SFTP_S_IRGRP|LIBSSH2_SFTP_S_IROTH);    
    FILE * infile = fopen(src_path,"rb");
    if (infile == NULL)
    {
        return -1;
    }
    if (!sftp_handle) {
#ifndef NDEBUG
        fprintf(stderr,"PUT: No handle\n");
#endif

        return -1;
    }
    do {
        char mem[1024];
 
        /* loop until we fail */ 
        rc = fread(mem,1,sizeof(mem),infile);
#ifndef NDEBUG
        fprintf(stderr,"PUT: %d \n", rc);
#endif        
        if (rc > 0) {
            rc = libssh2_sftp_write(sftp_handle, mem, rc);
        } else {
            break;
        }
    } while (1);
    fclose(infile);
    libssh2_sftp_close(sftp_handle);
    return (0);
}

int libsftp_get (libsftp_session *sftp, const char *src_path, const char *dst_path)
{
#ifndef NDEBUG
    fprintf(stderr,"GET %s -> %s\n", src_path, dst_path);
#endif
    LIBSSH2_SFTP *sftp_session = (LIBSSH2_SFTP *)sftp->libssh2_sftp_session;
    LIBSSH2_SFTP_HANDLE *sftp_handle;
    int rc;

    sftp_handle = libssh2_sftp_open(sftp_session, src_path, LIBSSH2_FXF_READ, 0);
    FILE * outfile = NULL;
    if (!sftp_handle) {
        return (-1);
    }
    outfile = fopen(dst_path,"wb");
    if (outfile == NULL)
    {
        libssh2_sftp_close(sftp_handle);
        return (-1);
    }
    do {
        char mem[1024];
 
        /* loop until we fail */
        rc = libssh2_sftp_read(sftp_handle, mem, sizeof(mem));

        if (rc > 0) {
            fwrite(mem,rc,1,outfile);
        } else {
            break;
        }
    } while (1);
    fclose(outfile);
    libssh2_sftp_close(sftp_handle);
    return (0);
}

libsftp_filelist *libsftp_opendir(libsftp_session *sftp, const char *path)
{
    LIBSSH2_SFTP *sftp_session = (LIBSSH2_SFTP *)sftp->libssh2_sftp_session;
    LIBSSH2_SFTP_HANDLE *sftp_handle;
    int rc;
    libsftp_filelist *current = NULL;
    libsftp_filelist *last = NULL;
    libsftp_filelist *list = NULL;
    sftp_handle = libssh2_sftp_opendir(sftp_session, path);    
    if (!sftp_handle) {
#ifndef NDEBUG
        fprintf(stderr, "Unable to open dir with SFTP\n");
#endif
        return NULL;
    }
    do {
        char mem[512];
        char longentry[512];
        LIBSSH2_SFTP_ATTRIBUTES attrs;
        
        /* loop until we fail */ 
        rc = libssh2_sftp_readdir_ex(sftp_handle, mem, sizeof(mem),
                                     longentry, sizeof(longentry), &attrs);
        if(rc > 0) 
        {
            current = malloc(sizeof(libsftp_filelist));
            current->filename = malloc(rc+1);
            strcpy(current->filename, mem);
            current->flags = attrs.flags;
            current->filesize = attrs.filesize;
            current->uid = attrs.uid; 
            current->gid = attrs.gid;
            current->permissions = attrs.permissions;
            current->atime = attrs.atime;
            current->mtime = attrs.mtime;
            current->next = NULL;
            if (last != NULL)
                last->next = current;
            if (list == NULL)
                list=current;
            last=current;
        }
        else
        {
            break;
        }
    } while (1);
 
    libssh2_sftp_closedir(sftp_handle);
    return list;
}


libsftp_session *libsftp_connect_pubkey(unsigned long hostaddr, const char *username, const char *publickey, const char *privatekey, const char *passphrase)
{
    LIBSSH2_SFTP *sftp_session = NULL;
    LIBSSH2_SESSION *session;
    const char *fingerprint;
    char *userauthlist;
    int rc;
    libsftp_session *sftp;

    sftp = libsftp_init_session(hostaddr);
    session = (LIBSSH2_SESSION *)sftp->libssh2_session;
        
    fingerprint = libssh2_hostkey_hash(session, LIBSSH2_HOSTKEY_HASH_SHA1);
    userauthlist = libssh2_userauth_list(session, username, strlen(username));
    if (libssh2_userauth_publickey_fromfile(session, username, publickey, privatekey, passphrase)) {
#ifndef NDEBUG
        fprintf(stderr, "\tAuthentication by public key failed!\n");
#endif
        libsftp_shutdown(sftp);
        return NULL;
    } 
    sftp_session = libssh2_sftp_init(session);
    if (!sftp_session) {
        libsftp_shutdown(sftp);
    }
#ifndef NDEBUG
    fprintf(stderr,"Session opened\n");
#endif
    sftp->libssh2_sftp_session = sftp_session;
    return sftp;
}

libsftp_session *libsftp_connect_password(unsigned long hostaddr, const char *username, const char *password)
{
    LIBSSH2_SFTP *sftp_session;
    LIBSSH2_SESSION *session;
    const char *fingerprint;
    char *userauthlist;
    int rc;

    libsftp_session *sftp;

    sftp = libsftp_init_session(hostaddr);
    session = (LIBSSH2_SESSION *)sftp->libssh2_session;

    fingerprint = libssh2_hostkey_hash(session, LIBSSH2_HOSTKEY_HASH_SHA1); 
    userauthlist = libssh2_userauth_list(session, username, strlen(username));
    if (libssh2_userauth_password(session, username, password)) {
#ifndef NDEBUG
        fprintf(stderr, "\tAuthentication by password failed!\n");
#endif
        libsftp_shutdown(sftp);
        return(NULL);
    }

    sftp_session = libssh2_sftp_init(session);
    if (sftp_session == NULL) {
        libsftp_shutdown(sftp);
    }
    sftp->libssh2_sftp_session = sftp_session;
    return (sftp);
}

void libsftp_disconnect(libsftp_session *sftp)
{
    if (sftp == NULL)
        return;
    LIBSSH2_SFTP *sftp_session = (LIBSSH2_SFTP *)sftp->libssh2_sftp_session;
    libssh2_sftp_shutdown(sftp_session);
    libsftp_shutdown(sftp);
    free(sftp);
}

